// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

const axios = require('axios');

module.exports = function (api) {
  api.loadSource(async ({ addCollection }) => {
    // Use the Data Store API here: https://gridsome.org/docs/data-store-api/
    const { data } = await axios.get(
      'https://rickandmortyapi.com/api/character',
    );

    const collection = addCollection({
      typeName: 'Characters',
    });

    for (const result of data.results) {
      collection.addNode({
        id: result.id,
        name: result.name,
        status: result.status,
        species: result.species,
        type: result.type,
        gender: result.gender,
        origin: {
          name: result.origin.name,
          url: result.origin.url,
        },
        location: {
          name: result.location.name,
          url: result.location.url,
        },
        image: result.image,
        episode: result.episode,
        url: result.url,
        created: result.created,
      });
    }
  });

  api.createPages(({ createPage }) => {
    // Use the Pages API here: https://gridsome.org/docs/pages-api/
  });
};
